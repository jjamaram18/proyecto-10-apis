from flask import Flask, jsonify
from flask_restful import Api
from .apis.mesasApi import *
from .apis.usuarioApi import *
from .apis.empleadoApi import *
from .db import db
import pymysql
import os

from .ext import ma, migrate
from flask_sqlalchemy import SQLAlchemy
dbSQL = SQLAlchemy()



def create_app(settings_module):
    app = Flask(__name__)
    app.config.from_object(settings_module)

    # Inicializa las extensiones
    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)
    
    # Generar la clave de sessión para crear una cookie con la inf. de la sessión
    app.config['SECRET_KEY'] = os.urandom(24)

    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://JesusAmaro:PanConQueso1466@JesusAmaro.mysql.pythonanywhere-services.com/comandas'
    
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    
    app.config['SECURITY_PASSWORD_SALT'] = 'thissecretsalt'
    
    app.config['CORS_HEADERS'] = 'Content-Type'


    dbSQL.init_app(app)

    @app.before_first_request
    def create_all():
        dbSQL.create_all()

    # Captura todos los errores 404
    Api(app, catch_all_404s=True)

    # Deshabilita el modo estricto de acabado de una URL con /
    app.url_map.strict_slashes = False

    # Registra los blueprints
    app.register_blueprint(mesas)
    app.register_blueprint(usuario)
    app.register_blueprint(empleado)
    

    # Registra manejadores de errores personalizados
    register_error_handlers(app)

    return app


def register_error_handlers(app):
    #@app.errorhandler(Exception)
    #def handle_exception_error(e):
    #    return jsonify({'msg': 'Internal server error'}), 500

    @app.errorhandler(405)
    def handle_405_error(e):
        return jsonify({'msg': 'Method not allowed'}), 405

    @app.errorhandler(403)
    def handle_403_error(e):
        return jsonify({'msg': 'Forbidden error'}), 403

    @app.errorhandler(404)
    def handle_404_error(e):
        return jsonify({'msg': 'Not Found error'}), 404

