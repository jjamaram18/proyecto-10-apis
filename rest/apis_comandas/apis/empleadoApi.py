from flask import request, Blueprint
from flask_restful import Api
from .schemas import EmpleadosSchema
from ..models import Empleados
from ..controlador.empleadoController import *

empleado = Blueprint('empleado', __name__, url_prefix='/empleado')

api = Api(empleado)

empleado_schema = EmpleadosSchema()

@empleado.route('/agregar', methods=['POST'])
def agregar():
    data = request.get_json(force=True)
    empleado = Empleados(**data)
    #agregarEmpleado(data['nombre'], data['apellidoPat'], data['apellidoMat'], data['correo'], data['telefono'], data['estatus'], data['usuario_id'], data['tipo'])
    empleado = empleado_schema.load(data)    
    #return empleado_schema.jsonify(empleado)
    return agregarEmpleado(data['nombre'], data['apellidoPat'], data['apellidoMat'], data['correo'], data['telefono'], data['estatus'], data['usuario_id'], data['tipo'])

@empleado.route('/modificar', methods=['PUT'])
def modificar():
    data = request.get_json(force=True)
    empleado = Empleados(**data)
    resp = modificarEmpleado(data['id'] ,data['nombre'], data['apellidoPat'], data['apellidoMat'], data['correo'], data['telefono'], data['estatus'], data['usuario_id'], data['tipo'])
    empleado = empleado_schema.load(data)
    #return empleado_schema.jsonify(empleado)
    return resp

@empleado.route('/eliminar', methods=['DELETE'])
def eliminar():
    data = request.get_json(force=True)
    empleado = Empleados(data['id'])
    resp = eliminarEmpleado(empleado.id)
    empleado = empleado_schema.load(data)
    #return empleado_schema.jsonify(empleado)
    return resp

@empleado.route('/listar', methods=['GET'])
def listar():
    empleados = listarEmpleados()
    return empleado_schema.jsonify(empleados, many=True)