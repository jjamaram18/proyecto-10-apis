from flask import request, Blueprint
from flask_restful import Api
from .schemas import MesasSchema
from ..models import Mesas
from ..controlador.mesasController import *

mesas = Blueprint('mesas', __name__, url_prefix='/mesas')

mesas_schema = MesasSchema()

api = Api(mesas)

@mesas.route('/agregar', methods=['POST'])
def agregar():
    data = request.get_json(force=True)
    #mesa = Mesas(**data)
    #agregarMesa(data.get('numero'), data.get('estatus'), data.get("capacidad"))    
    #return mesas_schema.jsonify(mesa)
    return agregarMesa(data.get('numero'), data.get('estatus'), data.get("capacidad"))

@mesas.route('/listar', methods=['GET'])
def listar():
    mesas = listarMesas()
    return mesas_schema.jsonify(mesas, many=True)

@mesas.route('/modificar', methods=['PUT'])
def modificar():
    data = request.get_json(force=True)
    #mesa = Mesas(data.get('id'), data.get('numero'), data.get('estatus'), data.get("capacidad"))
    
    return modificarMesas(data.get('id'), data.get('numero'), data.get('estatus'), data.get("capacidad"))

@mesas.route('/eliminar', methods=['DELETE'])
def eliminar():
    data = request.get_json(force=True)
    resp = eliminarMesa(data.get('id'))
    return resp