from marshmallow import fields

from ..ext import ma

    
class MesasSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    numero = fields.Integer()
    capacidad = fields.Integer()
    estatus = fields.Integer()    

class EmpleadosSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    nombre = fields.String()
    apellidoPat = fields.String()
    apellidoMat = fields.String()
    correo = fields.String()
    telefono = fields.String()
    usuario_id = fields.Integer()
    tipo = fields.Integer()
    estatus = fields.Integer()
    
class UsuariosSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    usuario = fields.String()
    password = fields.String()
    estatus = fields.Integer()
    tipo = fields.Integer()