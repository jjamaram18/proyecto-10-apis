from flask import request, Blueprint
from flask_restful import Api
from .schemas import UsuariosSchema
from ..models import Usuarios
from ..controlador.usuarioController import *

usuario = Blueprint('usuario', __name__, url_prefix='/usuario')

api = Api(usuario)

usuario_schema = UsuariosSchema()

@usuario.route('/agregar', methods=['POST'])
def agregar():
    data = request.get_json(force=True)
    resp = agregarUsuario(data['usuario'], data['password'], data['estatus'], data['tipo'])
    return resp

@usuario.route('/listar', methods=['GET'])
def listar():
    usuarios = listarUsuarios()
    return usuario_schema.jsonify(usuarios, many=True)

@usuario.route('/modificar', methods=['PUT'])
def modificar():
    data = request.get_json(force=True)
    resp = modificarUsuario(data['id'], data['usuario'], data['password'], data['estatus'], data['tipo'])
    return resp

@usuario.route('/eliminar', methods=['DELETE'])
def eliminar():
    data = request.get_json(force=True)
    resp = eliminarUsuario(data['id'])
    return resp