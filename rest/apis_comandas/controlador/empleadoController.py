from ..models import Empleados
from ..db import db
from ..__init__ import dbSQL

def agregarEmpleado(_nombre, _apellidoPat, _apellidoMat, _correo, _telefono, _estatus, _usuario_id, _tipo):
    empleado = Empleados(nombre = _nombre, apellidoPat = _apellidoPat, apellidoMat = _apellidoMat, correo = _correo, telefono = _telefono, estatus = _estatus, usuario_id = _usuario_id, tipo = _tipo)
    dbSQL.session.add(empleado)
    dbSQL.session.commit()
    return {'msg': 'Empleado agregado'}

def modificarEmpleado(id, nombre, apellidoPat, apellidoMat, correo, telefono, estatus, usuario_id, tipo):
    empleado = dbSQL.session.query(Empleados).filter_by(id=id).first()
    dbSQL.session.close()
    empleado.nombre = nombre
    empleado.apellidoPat = apellidoPat
    empleado.apellidoMat = apellidoMat
    empleado.correo = correo
    empleado.telefono = telefono
    empleado.estatus = estatus
    empleado.usuario_id = usuario_id
    empleado.tipo = tipo
    empleado.save()    
    return {'msg': 'Empleado modificado'}

def eliminarEmpleado(id):
    empleado = dbSQL.session.query(Empleados).filter_by(id=id).first()
    dbSQL.session.close()
    empleado.estatus = 0
    empleado.save()    
    return {'msg': 'Empleado eliminado'}

def listarEmpleados():
    empleados = dbSQL.session.query(Empleados).all()
    return empleados

