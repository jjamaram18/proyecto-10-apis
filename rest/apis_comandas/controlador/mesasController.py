from ..models import Mesas
from ..__init__ import dbSQL
from ..db import db
import random

def agregarMesa(_numero, _estatus, _capacidad):    
    try:        
        mesa = Mesas(numero=_numero, estatus=_estatus, capacidad=_capacidad)        
        dbSQL.session.add(mesa)
        dbSQL.session.commit()
        mesas = dbSQL.session.query(Mesas).all()
        id = mesas.__len__()    
        return {'msg':'Mesa agregada', 'id':id}
    except Exception as e:
        return {'msg': e.__str__()}

def listarMesas():
    mesas = dbSQL.session.query(Mesas).all()
    return mesas

def modificarMesas(_id, _numero, _estatus, _capacidad):
    try:
        mesa = dbSQL.session.query(Mesas).filter_by(id=_id).first()
        dbSQL.session.close()
        mesa.numero = _numero
        mesa.estatus = _estatus
        mesa.capacidad = _capacidad    
        mesa.save()
        return {'msg':'Mesa modificada', 'id':mesa.id, 'numero':mesa.numero, 'estatus':mesa.estatus, 'capacidad':mesa.capacidad}
    except Exception as e:
        return {'msg': e.__str__()}

def eliminarMesa(_id):
    mesa = dbSQL.session.query(Mesas).filter_by(id=_id).first()
    dbSQL.session.close()
    mesa.estatus = 0
    mesa.save()
    return {'msg':'Mesa eliminada'}