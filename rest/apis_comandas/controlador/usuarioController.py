from ..models import Usuarios
from ..db import db
from flask_sqlalchemy import SQLAlchemy
dbSQL = SQLAlchemy()

def agregarUsuario(_usuario, _password, _estatus, _tipo):
    try:
        usuario = Usuarios(usuario = _usuario, password = _password, estatus = _estatus, tipo = _tipo)
        dbSQL.session.add(usuario)
        dbSQL.session.commit()
        return {'msg': 'Usuario agregado'}
    except Exception as e:
        return {'msg': e.__str__()}

def listarUsuarios():
    try:
        usuarios = dbSQL.session.query(Usuarios).all()
        return usuarios
    except Exception as e:
        return {'msg': 'Error al listar usuarios'}

def modificarUsuario(id, _usuario, password, estatus, tipo):
    try:
        usuario = dbSQL.session.query(Usuarios).filter_by(id=id).first()
        dbSQL.session.close()
        usuario.usuario = _usuario
        usuario.password = password
        usuario.estatus = estatus
        usuario.tipo = tipo
        usuario.save()
        return {'msg': 'Usuario modificado'}
    except Exception as e:
        return {'msg': 'Error al modificar usuario ' + e.__str__()}

def eliminarUsuario(_id):
    try:
        usuario = dbSQL.session.query(Usuarios).filter_by(id=_id).first()
        dbSQL.session.close()
        usuario.estatus = 0
        usuario.save()
        return {'msg': 'Usuario eliminado'}
    except Exception as e:
        return {'msg': 'Error al eliminar usuario ' + e.__str__()}
    
    