from .db import db, BaseModelMixin

    
class Mesas(db.Model, BaseModelMixin):
    
    __tablename__ = 'mesas'
    id = db.Column(db.Integer, primary_key=True)
    numero = db.Column(db.Integer)
    estatus = db.Column(db.Integer, default=1)
    capacidad = db.Column(db.Integer)

        
    
    def __repr__(self):
        return f'Mesas({self.numero})'

    def __str__(self):
        return f'{self.numero}'

class Usuarios(db.Model, BaseModelMixin):
    
    __tablename__ = 'usuarios'
    id = db.Column(db.Integer, primary_key=True)    
    usuario = db.Column(db.String(50))
    password = db.Column(db.String(50))
    estatus = db.Column(db.Integer, default=1)
    tipo = db.Column(db.Integer)
            
    
    def __repr__(self):
        return f'Usuarios({self.usuario})'

    def __str__(self):
        return f'{self.usuario}'
    
class Empleados(db.Model, BaseModelMixin):
    
    __tablename__ = 'empleados'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(100))
    apellidoPat = db.Column(db.String(100))
    apellidoMat = db.Column(db.String(100))
    correo = db.Column(db.String(100))
    telefono = db.Column(db.String(100))
    estatus = db.Column(db.Integer, default=1)
    usuario_id = db.Column(db.Integer, db.ForeignKey('usuarios.id'))
    usuario = db.relationship('Usuarios', backref='empleados')
    tipo = db.Column(db.Integer)    


class Platillos(db.Model, BaseModelMixin):
    
    __tablename__ = 'platillos'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(100))
    precio = db.Column(db.Float)
    estatus = db.Column(db.Integer, default=1)
    tipo = db.Column(db.Integer)
        
    
    def __repr__(self):
        return f'Platillos({self.nombre})'

    def __str__(self):
        return f'{self.nombre}'