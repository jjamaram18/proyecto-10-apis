
@ECHO OFF
set FLASK_DEBUG=1

set FLASK_ENV=development

set FLASK_APP=apis_comandas

flask db init
flask db migrate
flask db upgrade
flask run 